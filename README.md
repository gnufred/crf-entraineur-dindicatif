# French Red Cross - radio operations, identifiers trainer

A simple, easy to use trainer for learning the French Red Cross radio operation identifiers. Uses ReactJS and Redux for core functionality. Uses React-Pose for some animations.

Forked from [NATO/IRSA Alphabet Trainer](https://github.com/cipherbeta/Phonetic-Alphabet-Trainer).

### How To Install

This app is built using CRA, so setup is relatively straightforward. Clone the repository to a directory of your choosing. Open your console in said directory and type `npm i` to install necessary dependencies. Once you're done, you can either type `npm run build` to generate a server-ready copy of the app in the build folder, or `npm start` to start up a development server that you can use to work on the app yourself.
