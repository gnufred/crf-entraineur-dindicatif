
const alphabet = [
    {
        "letter": "président / responsable",
        "code": "challenger"
    },
    {
        "letter": "directeur de l'urgence et du secourisme",
        "code": "topaze"
    },
    {
        "letter": "adjoint chargé de la formation",
        "code": "format"
    },
    {
        "letter": "adjoint chargé de l’urgence",
        "code": "laser"
    },
    {
        "letter": "adjoint chargé de l’animation des équipes",
        "code": "diamant"
    },
    {
        "letter": "adjoint chargé des missions de secours",
        "code": "action"
    },
    {
        "letter": "adjoint chargé des moyens opérationnels et des télécom",
        "code": "titane"
    },
    {
        "letter": "chef de secteur chargé des télécommunications",
        "code": "radar"
    },
    {
        "letter": "chef de secteur chargé de la logistique",
        "code": "cristal"
    },
    {
        "letter": "responsable de l’action solidarité",
        "code": "solidarité"
    },
    {
        "letter": "médecin",
        "code": "caducée"
    },
    {
        "letter": "infirmier",
        "code": "infirmier"
    },
    {
        "letter": "véhicule de premiers secours à personnes",
        "code": "vps"
    },
    {
        "letter": "véhicule de liaison",
        "code": "vl"
    },
    {
        "letter": "minibus",
        "code": "minibus"
    },
    {
        "letter": "véhicule de liaison tout-terrain",
        "code": "vltt"
    },
    {
        "letter": "véhicule logistique",
        "code": "log"
    },
    {
        "letter": "véhicule transmissions",
        "code": "pcm"
    },
    {
        "letter": "portatif",
        "code": "portatif"
    },
    {
        "letter": "base de la structure",
        "code": "base"
    },
    {
        "letter": "poste de coordination",
        "code": "pc"
    },
    {
        "letter": "service d’aide médical d’urgence",
        "code": "samu"
    },
    {
        "letter": "cadre d’astreinte opérationnelle",
        "code": "vigie"
    },
    {
        "letter": "élément léger d’évaluation et de commandement",
        "code": "elec"
    },
    {
        "letter": "responsable d’une opération",
        "code": "opéra"
    },
    {
        "letter": "centre de gestion des moyens du site",
        "code": "golf"
    },
    {
        "letter": "poste de secours",
        "code": "secours"
    },
    {
        "letter": "2ème équipe détachée du poste de secours",
        "code": "echo"
    },
    {
        "letter": "binôme détaché du poste de secours",
        "code": "sierra"
    },
    {
        "letter": "point d’alerte et de premiers secours",
        "code": "alpha"
    },
]

export default alphabet;
